<?php 

function db_connect()
{
    static $connection;

    if(!isset($connection)) {
        $config = parse_ini_file("config.ini");
        $connection = mysqli_connect($config['host'], $config['username'], $config['password'], $config['dbname']);
    }

    if($connection === false) {
        return mysqli_connect_error();
    }

    return $connection;
}

function db_query($query) {
    // connect to the database

    $connection = db_connect();

    $result = mysqli_query($connection, $query);

    return $result;
}

function db_select($query) {
    $result = db_query($query);

    if($result === false) {
        return false;
    }

    $rows = array();
    while($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}

function dd($var) {
    die(var_dump($var));
}
?>