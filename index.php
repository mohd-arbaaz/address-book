<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

        <!--Import Csutom CSS-->
        <link rel="stylesheet" href="css/style.css" type="text/css">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <title> Address Book </title>
    </head>

    <body>
        
        <!-- NAVIGATION BAR -->
        <nav>
            <div class="nav-wrapper">
            <a href="#!" class="brand-logo">Contact Info</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="#">Profile</a></li>
                <li><a href="#">Sign Out</a></li>
            </ul>
            </div>
        </nav>
        <!-- /NAVIGATION BAR -->

        <!-- Add a new Contact Button -->
        <div class="row mt50">
            <div class="col s12 right-align">
                <a href="#" class="btn waves-effect waves-light lighten-2 blue">
                    <i class="material-icons left">add</i> Add New
                </a>
            </div>
        </div>
        <!-- /Add a new Contact Button -->

        <!-- Table of Contacts -->
        <div class="row">
            <div class="col s12">
                <table class="highlight centered">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>DOB</th>
                            <th>Phone No.</th>
                            <th>Address</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img class="circle" src="images/users/antonio-freeman.jpg" alt="" height="78px" width="78px"></td>
                            <td>Mohd Arbaaz</td>
                            <td>someone@example.com</td>
                            <td>22-07-2000</td>
                            <td>1234567890</td>
                            <td>address</td>
                            <td><a href="#" class="btn btn-floating green lighten-2"><i class="material-icons">edit</i></a></td>
                            <td><a href="#deleteModal" class="btn btn-floating red lighten-2 modal-trigger delete-contact"><i class="material-icons">delete_forever</i></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Table of Contacts -->

        <!-- Footer -->
        <footer class="page-footer p0">
            <div class="footer-copyright">
                <div class="container">
                    <p class="center-align">&copy; 2021 Mohd Arbaaz</p>
                </div>
            </div>
        </footer>
        <!-- /Footer -->

        <!-- Delete Modal -->
        <div id="deleteModal" class="modal">
            <div class="modal-content">
                <h4>Delete Contact?</h4>
                <p>Are you sure you want to delete the contact?</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close btn blue-grey lighten-2 waves-effect">Cancel</a>
                <a href="#!" class="modal-close btn red lighten-2 waves-effect" id="modal-agree-button">Delete</a>
            </div>
        </div>
        <!-- /Delete Modal -->


        <!--JQuery Library-->
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <!--JavaScript at end of body for optimized loading-->
        <script type="text/javascript" src="js/materialize.min.js"></script>

        <!-- Page Level Scripts -->
        <script src="js/pages/index.js"></script>
    </body>
</html>